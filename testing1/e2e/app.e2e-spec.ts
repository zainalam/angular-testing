import { AngularTestingPage } from './app.po';

describe('angular-testing App', () => {
  let page: AngularTestingPage;

  beforeEach(() => {
    page = new AngularTestingPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
