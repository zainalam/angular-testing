import { Component } from '@angular/core';

@Component({
	selector: 'app-server',
	templateUrl: './server.component.html',
	styleUrls: ['./server.component.css']
})
export class ServerComponent {
	serverId: number = 10;
	serverStatus: string = 'offlineee';

	allowNewServer = false;
	serverCreationStatus = 'no server was created';
	serverName = 'test server';
	serverCreated = false;

	servers = ['test server 1', 'test server 2']

	constructor() {
		setTimeout( () => {
			this.allowNewServer = true;
		}, 2000 );

		this.serverStatus = Math.random() > 0.5 ? 'online' : 'offline';
	}

	getServerStatus() {
		return this.serverStatus + '3333333333';
	}

	onCreateServer() {
		this.serverCreated = true;
		this.servers.push(this.serverName);
		this.serverCreationStatus = 'Server is created ! Server name is '+ this.serverName;
	}

	onUpdateServerName(event: any) {
		console.log(event);
		// this.serverName = event.target.value;
		// or
		this.serverName = (<HTMLInputElement>event.target).value;
		
	}

	getColor() {
		return this.serverStatus === 'online' ? 'green' : 'red';
	}
}